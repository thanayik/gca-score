import os
import glob
from re import sub
import sys
import random
import csv
import argparse
from argparse import RawTextHelpFormatter
from pprint import pprint
from scipy.linalg import circulant
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import optimizer
import torchio as tio
import numpy as np
import nibabel as nib
from matplotlib import pyplot as plt
from pytorch_model_summary import summary
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split

# model = nn.Sequential(
    #     nn.Conv3d(1,32,3, padding='same'),
    #     nn.ReLU(),
    #     nn.Conv3d(32,64,3, padding='same'),
    #     nn.ReLU(),
    #     nn.MaxPool3d(3),
    #     nn.Conv3d(64,128,3, padding='same'),
    #     nn.ReLU(),
    #     nn.Conv3d(128,180,3, padding='same'), # reduce down the 256 to 180
    #     nn.ReLU(), 
    #     nn.MaxPool3d(3),
    #     nn.Flatten(),
    #     nn.Dropout(p=0.2),
    #     for non normalised data: 391680
    #     for normalised data: 307200
    #     nn.Linear(391680, 1024),
    #     nn.ReLU(),
    #     nn.Linear(1024, 512),
    #     nn.ReLU(),
    #     nn.Linear(512, 1),
    # )

def clip_high(img, thresh=75, fill_low=4, fill_high=9):
    '''
    deprecated -- not used anymore
    clip bright values above thresh and fill them in with csf-like values
    '''
    csf_like_value = random.randint(fill_low, fill_high)
    img[img >= thresh] = csf_like_value
    return img

def darken(in_tensor, thresh=50, low=0):
    '''
    darken voxels in input CT IMG (tensor or numpy array). Uses a piecewise linear function
    '''
    IMG = in_tensor.numpy()
    OUT = np.piecewise(IMG, [IMG < low, ((IMG >= low) & (IMG < thresh)), IMG >= thresh], [lambda x: x*0, lambda x: x, lambda x: -x + (thresh*2)])
    OUT[OUT < 0] = 0
    return torch.from_numpy(OUT)

class Flatten(nn.Module):
    def forward(self, x):
        x = x.view(x.size(0), -1)
        return x

def create_model(device, learning_rate=0.0001):
    '''
    create the pytorch model and return it along with the optimizer
    '''
    init_filter_num = 32
    max_pool_stride = 2
    network = nn.Sequential(
        # 1
        nn.Conv3d(1,init_filter_num, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.Conv3d(init_filter_num,init_filter_num, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.MaxPool3d(max_pool_stride),
        # 2
        nn.Conv3d(init_filter_num,init_filter_num *2, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.Conv3d(init_filter_num*2,init_filter_num*2, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.MaxPool3d(max_pool_stride),
        # 3
        nn.Conv3d(init_filter_num*2,init_filter_num *4, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.Conv3d(init_filter_num*4,init_filter_num*4, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.MaxPool3d(max_pool_stride),
        # 4
        nn.Conv3d(init_filter_num*4,init_filter_num *8, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.Conv3d(init_filter_num*8,init_filter_num*8, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.MaxPool3d(max_pool_stride),
        # 5
        nn.Conv3d(init_filter_num*8,init_filter_num *16, kernel_size=(3,3,3), padding='same'),
        nn.ReLU(),
        nn.Conv3d(init_filter_num*16,init_filter_num*16, kernel_size=(3,3,3), padding='same'), 
        nn.ReLU(),
        #nn.Dropout(), maybe add this in in the future to drop out filters
        nn.MaxPool3d(max_pool_stride),
        nn.Flatten(),
        nn.LazyLinear(1024),
        #nn.Linear(4608, 1024),
        nn.ReLU(),
        nn.Dropout(p=0.2),
        nn.Linear(1024, 256),
        nn.ReLU(),
        nn.Dropout(p=0.1),
        nn.Linear(256, 1),
    )
    # the GCA Model that worked well (85%)
   # network = nn.Sequential(
   #     nn.Conv3d(1,32, kernel_size=(11,11,11), padding='same'), # larger kernel size first
   #     nn.ReLU(),
   #     nn.MaxPool3d(3),
   #     nn.Conv3d(32,64,kernel_size=(7,7,7), padding='same'), # reduce kernel size
   #     nn.ReLU(),
   #     nn.MaxPool3d(3),
   #     nn.Conv3d(64,64,kernel_size=(5,5,5), padding='same'), # reduce kernel size again
   #     nn.ReLU(),
   #     nn.Conv3d(64, 64, kernel_size=(3,3,3), padding='same'),
   #     nn.ReLU(),
   #     nn.Conv3d(64, 128, kernel_size=(3,3,3), padding='same'),
   #     nn.ReLU(),
   #     nn.MaxPool3d(3),
   #     nn.Flatten(),
   #     nn.Linear(4608, 1024),
   #     nn.ReLU(),
   #     nn.Dropout(p=0.2),
   # #     for non normalised data: 1156680
   # #     for normalised data: 307200
   #     nn.Linear(1024, 256),
   #     nn.ReLU(),
   #     nn.Dropout(p=0.1),
   #     nn.Linear(256, 1),
   # )

    
    model = network.to(device)
    #print(model)
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate) 
    # uncomment to see summary of model printed to console.
    #print(summary(model, torch.zeros((1, 1, 157, 192, 170)), show_input=True))
    return model, optimizer

def write_loss_csv(file_path, losses):
  '''
  save loss list to a csv file
  '''
  if os.path.exists(file_path):
    os.unlink(file_path)
  with open(file_path, mode='a') as loss_file:
    csv_writer = csv.writer(loss_file, delimiter=',')
    csv_writer.writerow(['loss'])
    for loss in losses:
      csv_writer.writerow([loss])

def write_dataset_csv(file_path, subject_list):
  '''
  save subject list to a csv file. Useful for saving the list of subjects
  that create the train, val, and test datasets.
  '''
  if os.path.exists(file_path):
    os.unlink(file_path)
  with open(file_path, mode='a') as loss_file:
    csv_writer = csv.writer(loss_file, delimiter=',')
    csv_writer.writerow(['subject', 'gca_score'])
    for sub in subject_list:
      csv_writer.writerow(sub)


def infer(input_img, device, do_darken=1):
    '''
    estimate the GCA score of an untrained image. Will load the latest model available
    '''
    if not os.path.exists(input_img):
        return
    base_name = input_img.split('.nii')[0] # first first part of name without .nii or .nii.gz ext
    checkpoint = torch.load('../gca_model_099.pt') # load the pytorch data
    model, _ = create_model(device) # get the model
    model.load_state_dict(checkpoint['model_state_dict']) # initialize the model state
    model.eval() # make sure we are not in training mode
    subject_list = [] # mimics how things were done when trainging. 
    # create a new torchio subject from the input image
    subject = tio.Subject(
        ct=tio.ScalarImage(input_img) # named the key 'ct' here, but could be named anything as long as it is consistent between training and inference.
    )
    subject.plot() # plots an off screen plot of the middle slices in each plane. 
    plt.savefig(base_name + '.png') # save the plot without showing it
    subject_list.append(subject)
    # setup the list of torchio transforms to use on the input image. These should match the training transforms
    transforms = []
    if do_darken:
        transforms.append(tio.Lambda(darken))
    transforms.append(tio.RescaleIntensity(out_min_max=(0,1)))
    # crop or pad to (157, 192, 170)
    #transforms.append(tio.CropOrPad((157, 192, 170)))
    # compose them together
    transform = tio.Compose(transforms)
    # create subject dataset (torchio)
    classify_dataset = tio.SubjectsDataset(subject_list, transform=transform)
    # create  a data loader
    classify_loader = torch.utils.data.DataLoader(
        classify_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=1
    )
    # loop over subjects and use the model to predict GCA score
    for i, subject in enumerate(classify_loader): 
        inputs = subject['ct']['data'].to(device)
        inputs.requires_grad_()
        output = model(inputs) # run the input image through the model
        print(input_img, output[0][0].item(), flush=True) # prints the predicted GCA score to the console.
        output.backward()
        saliency = inputs.grad.abs() # creates the saliency matrix from the input image
        # don't threshold before saving anymore. instead, write out raw values. 
        #p95 = np.percentile(saliency, 95)
        #saliency[saliency < p95] = 0
        sub_nifti = nib.load(input_img) # load the original input image so that we can replace the image matrix but keep the nifti header info in our new image to save the saliency map
        # create the new nifti image for saliency
        saliency_nifti = nib.Nifti1Image(np.squeeze(saliency.numpy()), sub_nifti.affine, sub_nifti.header)
        nib.save(saliency_nifti, base_name + '_saliency.nii.gz') # save the saliency map

def train(train_dataset, val_dataset, device, batch_size=1, n_epochs=50, loss='mse', autosave=0):
    '''
    train the network
    '''
    model, optimizer = create_model(device)
    if loss == 'mse':
        criterion = nn.MSELoss()
    elif loss == 'mae':
        criterion = nn.L1Loss()
    training_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2,
        drop_last=True
    )
    val_loader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2,
        drop_last=True
    )

    loss_hist_train = [0] * n_epochs
    #acc_hist_train = [0] * n_epochs

    loss_hist_val = [0] * n_epochs
    #acc_hist_val = [0] * n_epochs

    
    for epoch in range(n_epochs):  # loop over the dataset multiple times
        train_scores = []
        train_predictions = []
        val_scores = []
        val_predictions = []
        #running_loss = 0.0
        model.train()
        for i, subject in enumerate(training_loader):
            # print subject index every 10 batches (if batch size is 1 this is each subject)
            if i % 10 == 0:
                print('Epoch: {}, Batch: {}'.format(epoch, i))
            # get the inputs; data is a list of [inputs, labels]
            inputs = subject['ct']['data'].to(device) # ct nifti images
            targets = subject['gca_score'].to(device) # gca scores
            #print(inputs.cpu().shape)
            outputs = model(inputs) # run inputs through network
            outputs = outputs.reshape(1, -1)
            outputs = torch.squeeze(outputs)
            targets = targets.reshape(1, -1)
            targets = torch.squeeze(targets)
            # print (targets)
            # if batch size is 1 to don't convert to list
            if batch_size == 1:
                train_scores.append(targets.item())
                train_predictions.append(outputs.item())
            else:
              train_scores.extend(targets.cpu().tolist())
              train_predictions.extend(outputs.cpu().tolist())

            loss = criterion(outputs.float(), targets.float())
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            loss_hist_train[epoch] += loss.item()  * inputs.size(0) # batch size
            #acc_hist_train[epoch] += 0#is_correct.sum()
            #print(f'[{epoch}, {i:5d}] loss: {running_loss:.3f}')
        loss_hist_train[epoch] /= len(training_loader.dataset)
        write_loss_csv('./loss_train.csv', loss_hist_train)
        model.eval()
        val_acc = []
        with torch.no_grad():
            for j, subject in enumerate(val_loader):
                # get the inputs; data is a list of [inputs, labels]
                inputs = subject['ct']['data'].to(device)
                targets = subject['gca_score'].to(device)
                # zero the parameter gradients
                # forward + backward + optimize
                outputs = model(inputs)
                outputs = outputs.reshape(1, -1)
                outputs = torch.squeeze(outputs)
                targets = targets.reshape(1, -1)
                targets = torch.squeeze(targets)
                # if batch size is 1 to don't convert to list
                if batch_size == 1:
                    val_scores.append(targets.item())
                    val_predictions.append(outputs.item())
                else:
                  val_scores.extend(targets.cpu().tolist())
                  val_predictions.extend(outputs.cpu().tolist())
                val_loss = criterion(outputs, targets.to(torch.float32))
                loss_hist_val[epoch] += val_loss.item() * inputs.size(0) #batch size
                #is_correct = 0 #((outputs>=0.5).float() == labels).float().cpu()
                #acc_hist_val[epoch] += 0#is_correct.sum()
                #running_loss += val_loss.item()
                #print(f'[{epoch}, {j:5d}] val_loss: {running_loss:.3f}')
            loss_hist_val[epoch] /= len(val_loader.dataset)
        write_loss_csv('./loss_val.csv', loss_hist_val) 
        print(f'Epoch {epoch} train loss: '
                f'{loss_hist_train[epoch]:.4f} val loss: '
                f'{loss_hist_val[epoch]:.4f}'
        )

        # update plot on disk
        x_arr = np.arange(len(loss_hist_train)) + 1
        fig = plt.figure(figsize=(12,4))
        ax = fig.add_subplot(1,2,1)
        ax.plot(x_arr, loss_hist_train, '-o', label='Train loss')
        ax.plot(x_arr, loss_hist_val, '--<', label='Val loss')
        ax.legend(fontsize=15)
        ax.set_xlabel('Epoch', size=15)
        ax.set_ylabel('Loss', size=15)
        plt.savefig('training_loss.png')
        plt.close(fig)
        # save checkpoint. These are 3.3GB each!!!
        # only saving the model and state info for each epoch. 
        # no need to save the "best" model, or the latest, because
        # that can be inferred by loading the saved dicts and 
        # from loading the model with the highest epoch number (latest)
        if autosave:
            torch.save({
              'epoch': epoch,
              'model_state_dict': model.state_dict(),
              'optimizer_state_dict': optimizer.state_dict(),
              'train_loss': loss_hist_train,
              'val_loss': loss_hist_val,
              'train_scores': train_scores,
              'train_predictions': train_predictions,
              'val_scores': val_scores,
              'val_predictions': val_predictions
              }, f'gca_model_{epoch:03}.pt')
    # save the final model no matter what
    torch.save({
      'epoch': epoch,
      'model_state_dict': model.state_dict(),
      'optimizer_state_dict': optimizer.state_dict(),
      'train_loss': loss_hist_train,
      'val_loss': loss_hist_val,
      'train_scores': train_scores,
      'train_predictions': train_predictions,
      'val_scores': val_scores,
      'val_predictions': val_predictions
      }, f'gca_model_{epoch:03}.pt')
    print('Finished Training')


def get_csv_data(csv_input_file):
    '''
    read a csv file and return the list of data
    '''
    with open(csv_input_file) as f:
        reader = csv.reader(f)
        data = list(reader)
    return data[1:]


def create_dataset(csv_file, data_path, do_darken=0):
    '''
    create a TorchIO dataset of TorchIO subjects
    '''
    subject_files = []
    subject_scores = []
    data = get_csv_data(csv_file)
    for d in data:
        # if the scanDate field is blank (true for the healthy atrophy subjects)
        if d[1] == '':
            ct_file = os.path.join(
                os.path.abspath(data_path),
                'sub-' + str(d[0]) + '_CT_reoriented2std_rfov_brain_tostandard.nii.gz'
            )
        # else stroke subjects
        else:
            ct_file = os.path.join(
                os.path.abspath(data_path),
                str(d[0]) + '_' + str(d[1]) + '_CT_reoriented2std_rfov_brain_tostandard.nii.gz'
            )
        if os.path.exists(ct_file):
            subject_files.append(ct_file)
            subject_scores.append(float(d[2]))
    subzip = list(zip(subject_files, subject_scores))
    
    scores = []
    for sub_score_combo in subzip:
        scores.append(int(sub_score_combo[1]))
    scores = np.array(scores)
    hist, bin_edges = np.histogram(scores, bins=5)
    bins = []
    for sub_score_combo in subzip:
        score = int(sub_score_combo[1])
        if score >= bin_edges[0] and score < bin_edges[1]:
            bins.append(1)
        elif score >= bin_edges[1] and score < bin_edges[2]:
            bins.append(2)
        elif score >= bin_edges[2] and score < bin_edges[3]:
            bins.append(3)
        elif score >= bin_edges[3] and score < bin_edges[4]:
            bins.append(4)
        elif score >= bin_edges[4] and score <= bin_edges[5]:
            bins.append(5)
    
    X_train, X_test, bins_train, bins_test = train_test_split(subzip, bins, test_size=0.4, random_state=42, stratify=bins)
    X_val, X_test, bins_val, bins_test = train_test_split(X_test, bins_test, test_size=0.5, random_state=42, stratify=bins_test)
    train_subject_list = []
    train_subject_files = []
    for subject in X_train:
        train_subject_files.append([subject[0], subject[1]])
        sub = tio.Subject(
            ct=tio.ScalarImage(subject[0]),
            gca_score=subject[1]
        )
        train_subject_list.append(sub)
    write_dataset_csv(os.path.join(os.path.dirname(csv_file), 'training_subject_files.csv'), train_subject_files)
    val_subject_list = []
    val_subject_files = []
    for subject in X_val:
        val_subject_files.append([subject[0], subject[1]])
        sub = tio.Subject(
            ct=tio.ScalarImage(subject[0]),
            gca_score=subject[1]
        )
        val_subject_list.append(sub)
    write_dataset_csv(os.path.join(os.path.dirname(csv_file), 'validation_subject_files.csv'), val_subject_files)
    test_subject_list = []
    test_subject_files = []
    for subject in X_test:
        test_subject_files.append([subject[0], subject[1]])
        sub = tio.Subject(
            ct=tio.ScalarImage(subject[0]),
            gca_score=subject[1]
        )
        test_subject_list.append(sub)
    write_dataset_csv(os.path.join(os.path.dirname(csv_file), 'test_subject_files.csv'), test_subject_files)
    all_transforms = []
    if do_darken:
        all_transforms.append(tio.Lambda(darken))
    all_transforms.append(tio.RescaleIntensity(out_min_max=(0,1))) 
    # crop or pad to (157, 192, 170), the output of utility_scripts/find_largest_dims.py
    #all_transforms.append(tio.CropOrPad((157, 192, 170)))

    composed_transform = tio.Compose(all_transforms)
    train_dataset = tio.SubjectsDataset(train_subject_list, transform=composed_transform)
    val_dataset = tio.SubjectsDataset(val_subject_list, transform=composed_transform)
    test_dataset = tio.SubjectsDataset(test_subject_list, transform=composed_transform)

    
    return [train_dataset, val_dataset, test_dataset]

    
def main():
    '''
    run the main program if invoked from the CLI
    '''
    description = f'FSL GCA. Estimate the global cortical atrophy score from a CT brain image.\n\nExample (training): python fsl_gca.py --mode train --device gpu --csvfile <somefile.csv> --datadir /path/to/data\n\nExample (inference): python fsl_gca.py --mode inference --device cpu --input /path/to/some_nifti_image.nii.gz'
    parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)
    parser.add_argument('--mode', default='inference', type=str, help='specify train or inference. default is inference')
    parser.add_argument('--device', default='cpu', type=str, help='specify cpu or gpu. Training will default to gpu. Inference will default to cpu.')
    parser.add_argument('--csvfile', type=str, help='the main csv file containing subjects and GCA scores')
    parser.add_argument('--datadir', type=str, help='the path to the directory containing the input subject images')
    parser.add_argument('--input', type=str, default='', help='the input file to run inference on. Only used when not training')
    parser.add_argument('--epochs', type=int, default=100, help='number of epochs to train for')
    parser.add_argument('--batchsize', type=int, default=1, help='the batch size')
    parser.add_argument('--loss', type=str, default='mse', help='the pytorch loss function to use. Options are: mse, mae')
    parser.add_argument('--autosave', type=int, default=0, help='autosave checkpoints while training (uses lots of disk space)')
    parser.add_argument('--darken', default=1, type=int, help="darken abnormall bright voxels that can be excess skull or calcifications in CT images")
    args = parser.parse_args()
    mode = args.mode
    chip = args.device
    real_device = chip
    do_darken = args.darken
    if chip == 'gpu':
        real_device = 'cuda:1'
    device = torch.device(real_device)
    if mode == 'train':
        if args.input != '':
            raise Exception('--input should not be used with --mode train')
        csv_file = args.csvfile
        data_path = args.datadir
        batch_size = args.batchsize
        n_epochs = args.epochs
        loss_function = args.loss
        autosave=args.autosave
        train_dataset, val_dataset, test_dataset = create_dataset(csv_file,data_path, do_darken=do_darken)
        train(train_dataset=train_dataset, val_dataset=val_dataset, device=device, batch_size=batch_size, n_epochs=n_epochs, loss=loss_function, autosave=autosave)
    elif mode == 'inference':
        input_img = args.input
        infer(input_img, device, do_darken=do_darken)

if __name__ == "__main__":
    '''
    run whatever the user specified
    '''
    main()
