import os
import csv
import subprocess
import sys
import datetime

def read_csv_file(filename):
    """
    Reads a csv file and returns a list of lists.
    """
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        return list(reader)

def write_csv_file(filename, data):
    """
    Writes a csv file.
    """
    with open(filename, 'a') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(data)

def run_command(command):
    """
    Runs a command and returns the output.
    """
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    return output.decode("utf-8") , error

def generate_predictions(test_set_csv_file):
    """
    Generates predictions for the test set.
    """
    # get date and time for the output file
    date_time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    # Read the test set
    test_set = read_csv_file(test_set_csv_file)

    # Iterate over the test set and generate predictions
    for row in test_set:
        # Get the image id
        image_path = row[0]
        # Get the image GCA
        image_gca = row[1]
        # Generate predictions
        command = 'python fsl_gca.py --input {}'.format(image_path)
        output, error = run_command(command)
        parts = output.split(' ')
        # Get the predicted GCA
        predicted_gca = parts[1]
        # write the output to a csv file
        new_row = [image_path, image_gca, predicted_gca]
        write_csv_file(f'predictions_{date_time}.csv', [new_row])
        # Print the output
        print(new_row)

def main():
    # Get the test set csv file as the first argument
    test_set_csv_file = sys.argv[1]
    # Generate predictions
    generate_predictions(test_set_csv_file)

if __name__ == '__main__':
  main()
