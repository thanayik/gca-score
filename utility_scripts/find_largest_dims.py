import nibabel as nib
import glob
import sys
import os

def get_dims(img):
    """get the dimensions of a 3D image.

    Parameters
    ----------
    img : str
        Path to a 3D image.

    Returns
    -------
    dims : tuple
    """
    img = nib.load(img)
    # set image to stndard orientation
    img = nib.as_closest_canonical(img)
    data = img.get_fdata()
    dims = data.shape
    return dims

def read_all_img_dims(imgs):
    """Read the dimensions of a list of images.

    Parameters
    ----------
    imgs : list
        List of paths to images.

    Returns
    -------
    dims : list
        List of tuples of image dimensions.
    """
    dims = []
    for img in imgs:
        dims.append(get_dims(img))
    return dims

def get_largest_dims(imgs):
    """Get the largest dimensions of a list of images.

    Parameters
    ----------
    imgs : list
        List of paths to images.

    Returns
    -------
    dims : tuple
        Largest dimensions of images.
    """
    dims = read_all_img_dims(imgs)
    largest_dims = [0, 0, 0]
    for dim in dims:
        if dim[0] > largest_dims[0]:
            largest_dims[0] = dim[0]
        if dim[1] > largest_dims[1]:
            largest_dims[1] = dim[1]
        if dim[2] > largest_dims[2]:
            largest_dims[2] = dim[2]
    return tuple(largest_dims)

if __name__ == '__main__':
    img_dir = sys.argv[1]
    # list all .nii.gz files in a directory
    imgs = glob.glob(os.path.join(img_dir, '*.nii.gz'))
    largest_dims = get_largest_dims(imgs)
    print(largest_dims) # (157, 192, 170)