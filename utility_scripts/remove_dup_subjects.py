import csv
import os
import sys
def get_csv_data(csv_input_file):
    '''
    read a csv file and return the list of data
    '''
    with open(csv_input_file) as f:
        reader = csv.reader(f)
        data = list(reader)
    return data[1:]

def write_csv(file_path, rows):
    '''
    save a new csv file with duplicates removed
    '''
    ids = []
    if os.path.exists(file_path):
        os.unlink(file_path)
    with open(file_path, mode='a') as loss_file:
        csv_writer = csv.writer(loss_file, delimiter=',')
        csv_writer.writerow(['id', 'scanDate', 'score'])
        for row in rows:
            id = row[0].rstrip()
            if id not in ids:
                csv_writer.writerow(row)
            ids.append(id)

if __name__ == '__main__':
    csv_file = sys.argv[1]
    out_path = sys.argv[2]
    data = get_csv_data(os.path.abspath(csv_file))
    write_csv(os.path.abspath(out_path), data)

