import os
import sys
import csv
from subprocess import run
from ct_bet import ct_bet
import nibabel as nib
import numpy as np

FSLDIR = os.getenv("FSLDIR")  
FSLDIR_BIN = os.path.join(FSLDIR, 'bin') 

def robustfov(input_file, output_file=None):
    '''
    run fsl's robustfov program
    '''
    if output_file is None:
        output_file = input_file.split('.nii')[0] + '_rfov.nii.gz'

    cmd = [
        os.path.join(FSLDIR_BIN, 'robustfov'),
        '-i',
        input_file,
        '-r',
        output_file,
    ]
    run(" ".join(cmd), shell=True, check=True)
    return output_file

def reorient2std(input_file, output_file=None):
    '''
    run fsl's reorient2std program
    '''
    if output_file is None:
        output_file = input_file.split('.nii')[0] + '_reoriented2std.nii.gz'

    cmd = [
        os.path.join(FSLDIR_BIN, 'fslreorient2std'),
        input_file,
        output_file,
    ]
    run(" ".join(cmd), shell=True, check=True)
    return output_file

def bet(input_file, output_file=None):
    '''
    run ct_bet
    '''
    if output_file is None:
        output_file = input_file.split('.nii')[0] + '_brain.nii.gz'
    ct_bet(input_file, out_pth=output_file, f_val=0.1, make_mask=True, erode=True, save_units=True, remove_bone=True)
    return output_file

def register(input_file, output_file=None):
    '''
    run fsl's flirt linear registration program
    '''
    if output_file is None:
        output_file = input_file.split('.nii')[0] + '_tostandard.nii.gz'
    
    omat = output_file.split('.nii')[0] + '.mat'
    
    ref = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'scct_stripped_mni.nii.gz'
    )

    cmd = [
        os.path.join(FSLDIR_BIN, 'flirt'),
        '-in',
        input_file,
        '-out',
        output_file,
        '-ref',
        ref,
        '-omat',
        omat,
    ]
    run(" ".join(cmd), shell=True, check=True)
    return output_file

def regions2files(input_file):
    '''
    input_file is a CT image that is registered to the template.

    Each GCA atlas region will be saved to its own file for model training 
    '''
    frontal = 3
    parietal = 6
    occipital = 5
    temporal = 8
    atlas_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'MNI-maxprob-thr50-2mm.nii.gz'
    )
    atlas_img = nib.load(atlas_path)
    atlas_data = atlas_img.get_fdata()
    # round the atlas data to integers
    atlas_data = np.round(atlas_data)
    # split atlas data into Left and Right based on dim 0 midpoint
    atlas_data_L = atlas_data.copy()
    atlas_data_R = atlas_data.copy()
    atlas_data_R[atlas_data_R.shape[0]//2:, :, :] = 0
    atlas_data_L[:atlas_data_L.shape[0]//2, :, :] = 0

    vent_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'HarvardOxford-2mm-latvent-dilated.nii.gz'
    )
    vent_img = nib.load(vent_path)
    vent_data = vent_img.get_fdata()

    input_img = nib.load(input_file)
    input_data = input_img.get_fdata()

    frontal_data_L = np.zeros_like(input_data)
    parietal_occip_data_L = np.zeros_like(input_data)
    temporal_data_L = np.zeros_like(input_data)
    frontal_data_R = np.zeros_like(input_data)
    parietal_occip_data_R = np.zeros_like(input_data)
    temporal_data_R = np.zeros_like(input_data)
    vent_out_data = np.zeros_like(input_data)

    frontal_data_L[atlas_data_L == frontal] = input_data[atlas_data_L == frontal]
    parietal_occip_data_L[atlas_data_L == parietal] = input_data[atlas_data_L == parietal]
    parietal_occip_data_L[atlas_data_L == occipital] = input_data[atlas_data_L == occipital]
    temporal_data_L[atlas_data_L == temporal] = input_data[atlas_data_L == temporal]
    vent_out_data[vent_data > 0] = input_data[vent_data > 0]

    frontal_data_R[atlas_data_R == frontal] = input_data[atlas_data_R == frontal]
    parietal_occip_data_R[atlas_data_R == parietal] = input_data[atlas_data_R == parietal]
    parietal_occip_data_R[atlas_data_R == occipital] = input_data[atlas_data_R == occipital]
    temporal_data_R[atlas_data_R == temporal] = input_data[atlas_data_R == temporal]

    
    # ensure we remove zeros to allow fslroi with fslstats -w to work
    # ensures all regional files are the same dims
    frontal_data_L[atlas_data_L == frontal] = frontal_data_L[atlas_data_L == frontal] +1
    parietal_occip_data_L[atlas_data_L == parietal] = parietal_occip_data_L[atlas_data_L == parietal] +1
    parietal_occip_data_L[atlas_data_L == occipital] = parietal_occip_data_L[atlas_data_L == occipital] +1
    temporal_data_L[atlas_data_L == temporal] = temporal_data_L[atlas_data_L == temporal] +1
    vent_out_data[vent_data > 0] = vent_out_data[vent_data > 0] +1
    frontal_data_R[atlas_data_R == frontal] = frontal_data_R[atlas_data_R == frontal] +1
    parietal_occip_data_R[atlas_data_R == parietal] = parietal_occip_data_R[atlas_data_R == parietal] +1
    parietal_occip_data_R[atlas_data_R == occipital] = parietal_occip_data_R[atlas_data_R == occipital] +1
    temporal_data_R[atlas_data_R == temporal] = temporal_data_R[atlas_data_R == temporal] +1

    frontal_img_L = nib.Nifti1Image(frontal_data_L, atlas_img.affine, atlas_img.header)
    frontal_file_L = input_file.split('.nii')[0] + '_frontal_L.nii.gz'
    nib.save(frontal_img_L, frontal_file_L)

    frontal_img_R = nib.Nifti1Image(frontal_data_R, atlas_img.affine, atlas_img.header)
    frontal_file_R = input_file.split('.nii')[0] + '_frontal_R.nii.gz'
    nib.save(frontal_img_R, frontal_file_R)

    parietal_occip_img_L = nib.Nifti1Image(parietal_occip_data_L, atlas_img.affine, atlas_img.header)
    parietal_occip_file_L = input_file.split('.nii')[0] + '_parietal_occip_L.nii.gz'
    nib.save(parietal_occip_img_L, parietal_occip_file_L)

    parietal_occip_img_R = nib.Nifti1Image(parietal_occip_data_R, atlas_img.affine, atlas_img.header)
    parietal_occip_file_R = input_file.split('.nii')[0] + '_parietal_occip_R.nii.gz'
    nib.save(parietal_occip_img_R, parietal_occip_file_R)

    temporal_img_L = nib.Nifti1Image(temporal_data_L, atlas_img.affine, atlas_img.header)
    temporal_file_L = input_file.split('.nii')[0] + '_temporal_L.nii.gz'
    nib.save(temporal_img_L, temporal_file_L)

    temporal_img_R = nib.Nifti1Image(temporal_data_R, atlas_img.affine, atlas_img.header)
    temporal_file_R = input_file.split('.nii')[0] + '_temporal_R.nii.gz'
    nib.save(temporal_img_R, temporal_file_R)

    vent_out_img = nib.Nifti1Image(vent_out_data, atlas_img.affine, atlas_img.header)
    vent_out_file = input_file.split('.nii')[0] + '_vent.nii.gz'
    nib.save(vent_out_img, vent_out_file)

    # now call fslstats and fslroi to make ROIs from the regions (smaller files)
    # fslstats <input> -w 
    # fslroi <input> <output> <xmin> <xsize> <ymin> <ysize> <zmin> <zsize> <tmin> <tsize>
    # loop over the files and make the commands

    files_to_process = [
        frontal_file_L,
        parietal_occip_file_L,
        temporal_file_L,
        vent_out_file,
        frontal_file_R,
        parietal_occip_file_R,
        temporal_file_R,
    ]

    for f in files_to_process:

        cmd = [
            os.path.join(FSLDIR_BIN, 'fslstats'),
            f,
            '-w',
        ]
        # run the command and get stdout
        output = run(" ".join(cmd), shell=True, check=True, capture_output=True).stdout
        # make sure output is a string
        output = output.decode('utf-8')

        # now make the command for fslroi
        cmd = [
            os.path.join(FSLDIR_BIN, 'fslroi'),
            f,
            f,
            output
        ]
        run(" ".join(cmd), shell=True, check=True)

    return files_to_process

    
def get_csv_data(csv_input_file):
    '''
    read a csv file and return the list of data
    '''
    with open(csv_input_file) as f:
        reader = csv.reader(f)
        data = list(reader)
    return data[1:]

def commands2file(input_file, save_file):
    '''
    save preprocessing commands to text file for 
    fsl_sub batch processing
    '''
    this_file = os.path.abspath(__file__)
    command = f'fslpython {this_file} run {input_file}{os.linesep}'
    with open(save_file, "a") as sf:
        sf.write(command)

def process_file(input_file):
    '''
    run the preprocessing image transform routines
    '''
    out = reorient2std(input_file)
    out = robustfov(out)
    out = bet(out)
    out = register(out)
    # regions2files(out)

def main():
    '''
    the main program to run from the CLI
    '''
    prepare_or_run = sys.argv[1]
    if 'prepare' == prepare_or_run:
        # e.g. fslpython preprocess.py prepare file.csv dataset fsl_sub.txt
        # csv_file = sys.argv[2]
        datadir = sys.argv[2]
        sub_file = sys.argv[3]
        # data = get_csv_data(csv_file)
        sub_file = os.path.abspath(sub_file)
        if os.path.exists(sub_file):
            os.unlink(sub_file)

        # for d in data:
        #     ct_file = os.path.join(
        #         os.path.abspath(datadir),
        #         str(d[0]) + '_' + str(d[1]) + '_CT.nii.gz'
        #     )
        #     if os.path.exists(ct_file):
        #         commands2file(ct_file, sub_file)

        # list datadir and get all files
        files = os.listdir(os.path.abspath(datadir))
        for f in files:
            if f.endswith('.nii.gz'):
                commands2file(
                    os.path.join(os.path.abspath(datadir), f),
                    sub_file
                )

    elif 'run' == prepare_or_run:
        # e.g. fslpython preprocess.py run /some/file.nii.gz
        process_file(sys.argv[2])

if __name__ == '__main__':
    main()