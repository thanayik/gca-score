# find files from Emma's GCA scoring sheet
import os
import sys
import pandas as pd

def get_file_paths(text_file):
    """Get file paths from text file."""
    file_paths = []
    with open(text_file, 'r') as f:
        for line in f:
            file_paths.append(line.rstrip())
    return file_paths

def get_score_df(csv_file, remove_dups=True):
    """Get score dataframe from csv file."""
    df = pd.read_csv(csv_file, dtype=str)
    # print the number of rows
    print(len(df))
    if remove_dups:
        df = df.drop_duplicates(subset=['ID'])
        # print the number of rows
        print(len(df))
    return df

def filter_files(file_paths, df, col_name):
    '''
    Filter files based on subtring matching by the given col_name in the dataframe

    '''
    valid_files = []
    for file_path in file_paths:
        for index, row in df.iterrows():
            name_parts = row[col_name].split('.nii')
            nifti_name = name_parts[0]
            ext = name_parts[1]
            if nifti_name in file_path:
                # get dirname
                dir_name = os.path.dirname(file_path)
                # construct nifti path from dir_name and base_name with new ext
                nifti_path = os.path.join(dir_name, row[col_name])
                # if nifti_path does not end in .gz then add it
                if not nifti_path.endswith('.gz'):
                    nifti_path = nifti_path + '.gz'
                notes = str(row['notes'])
                # Emma uses STR to indicate stroke or something similar in appearance. 
                # We want to exclude these files for the first GCA model
                # if 'str' in notes.lower():
                    # valid_files.append(nifti_path)
                valid_files.append(nifti_path)
    print(len(valid_files))
    return valid_files


def write_lines(file_paths, output_file):
    """Write file paths to output file."""
    with open(output_file, 'w') as f:
        for file_path in file_paths:
            f.write(file_path + '\n')


def main():

    # parse args
    file_paths = sys.argv[1]
    csv_file = sys.argv[2]
    # get the list of file paths from the text file
    file_paths = get_file_paths(os.path.abspath(file_paths))
    # get the score dataframe from the csv file
    df = get_score_df(os.path.abspath(csv_file))
    # filter files
    valid_files = filter_files(file_paths, df, 'file_brain')
    # write to output file
    output_file = 'valid_files.txt'
    write_lines(valid_files, output_file)

if __name__ == '__main__':
    main()