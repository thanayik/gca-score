import os
import sys
import pandas as pd
import numpy as np
import nibabel as nib

def main():
    csv_file = sys.argv[1]
    data_dir = sys.argv[2]

    df = pd.read_csv(csv_file)

    data_files = os.listdir(data_dir)
    data_files = [f for f in data_files if f.endswith('.nii.gz')]
    # remove files that start with . 
    data_files = [f for f in data_files if not f.startswith('.')]
    # remove .nii.gz and .nii extensions from data_files
    data_files = [f.replace('.nii.gz', '') for f in data_files]
    data_files = [f.replace('.nii', '') for f in data_files]
    # remove _reoriented2std_rfov_brain_tostandard from data_files
    data_files = [f.replace('_reoriented2std_rfov_brain_tostandard', '') for f in data_files]
    print('data_files length: {}'.format(len(data_files)))


    # loop over data files and check if that file is in the nifti_name column
    # if not, then drop that row
    # first, remove the .nii or .nii.gz file extension from the nifti_name column
    df['nifti_name'] = df['nifti_name'].str.replace('.nii.gz', '')
    df['nifti_name'] = df['nifti_name'].str.replace('.nii', '')

    # loop over data_files
    # the loop over the rows in the dataframe
    # if the data_file matches the nifti_name, then keep that row
    # if not, then drop that row
    # keep track of the rows to drop in a list
    rows_to_drop = []
    for index, row in df.iterrows():
        nifti_name = row['nifti_name']
        if nifti_name not in data_files:
            rows_to_drop.append(index)
    print('rows_to_drop length: {}'.format(len(rows_to_drop)))
    # drop the rows
    df = df.drop(rows_to_drop)
    print('df length: {}'.format(len(df)))

    # are there any duplicates?
    # check the ID column
    # if there are duplicates, then drop them
    df = df.drop_duplicates(subset=['ID'])
    print('df length: {}'.format(len(df)))

    # as a final check, make sure each nifti_name in the final dataframe
    # has a file that exists in the data_dir

    # loop over the nifti_name column
    # check if that file exists in the data_dir
    # if not, then drop that row
    # keep track of the rows to drop in a list
    rows_to_drop = []
    
    for index, row in df.iterrows():
        nifti_name = row['nifti_name']
        nifti_path = os.path.join(data_dir, nifti_name + '_reoriented2std_rfov_brain_tostandard.nii.gz')
        if not os.path.exists(nifti_path):
            rows_to_drop.append(index)

    print('rows_to_drop length: {}'.format(len(rows_to_drop)))
    # drop the rows
    df = df.drop(rows_to_drop)

    # add _reoriented2std_rfov_brain_tostandard.nii.gz to the nifti_name column
    df['nifti_name'] = df['nifti_name'] + '_reoriented2std_rfov_brain_tostandard.nii.gz'

    # finally sum the v_frontal_horn_L, v_frontal_horn_R, v_occipital_horn_L, v_occipital_horn_R, v_temporal_horn_L, v_temporal_horn_R columns into a new column called lat_vent_total
    df['lat_vent_total'] = df['v_frontal_horn_L'] + df['v_frontal_horn_R'] + df['v_occipital_horn_L'] + df['v_occipital_horn_R'] + df['v_temporal_horn_L'] + df['v_temporal_horn_R']

    # rename nifti_name to file_brain
    df.rename(columns={'nifti_name': 'file_brain'}, inplace=True)
    
    # make new columns for file_frontal_L, file_frontal_R, file_parietal_occip_L, file_parietal_occip_R, file_temporal_L, file_temporal_R, file_lat_vent
    # use the file_brain column for the basename and remove .nii.gz
    # df['file_frontal_L'] = df['file_brain'].str.replace('.nii.gz', '_frontal_L.nii.gz')
    # df['file_frontal_R'] = df['file_brain'].str.replace('.nii.gz', '_frontal_R.nii.gz')
    # df['file_parietal_occip_L'] = df['file_brain'].str.replace('.nii.gz', '_parietal_occip_L.nii.gz')
    # df['file_parietal_occip_R'] = df['file_brain'].str.replace('.nii.gz', '_parietal_occip_R.nii.gz')
    # df['file_temporal_L'] = df['file_brain'].str.replace('.nii.gz', '_temporal_L.nii.gz')
    # df['file_temporal_R'] = df['file_brain'].str.replace('.nii.gz', '_temporal_R.nii.gz')
    # df['file_lat_vent'] = df['file_brain'].str.replace('.nii.gz', '_vent.nii.gz')

    # save the dataframe to a csv file
    df.to_csv('./master_file_more_orchard_data.csv', index=False)

    # for each file in file_frontal_L check the nifti dimensions and make sure they are the same
    # if not, then print the file name

    # shape = []
    # for index, row in df.iterrows():
    #     file_path = os.path.join('/Volumes/PortableSSD/data/gca/gca_scored_data_regions', row['file_lat_vent'])
    #     img = nib.load(file_path)
    #     current_shape = img.shape
    #     if current_shape != shape:
    #         print(file_path)
    #         print(current_shape)
    #         shape = current_shape



    


if __name__ == '__main__':
    main()